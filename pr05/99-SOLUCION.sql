-- 1. Elabora un listado (sin repeticiones) con los apellidos de los clientes de la empresa que hayan hecho algún 
--    pedido online (order_mode online) junto con el apellido del empleado que gestiona su cuenta. Muestra en el 
--    listado primero el apellido del empleado que gestiono la cuenta y luego el apellido del cliente, y haz que el
--    listado se encuentre ordenado por apellido de empleado primero y luego por apellido del cliente. Usa reuniones (JOIN)
--    para ello.

SELECT DISTINCT last_name, cust_last_name
FROM pr5_customers JOIN pr5_orders USING (customer_id) JOIN pr5_employees ON (account_mgr_id = employee_id)
WHERE ORDER_MODE = 'online'
ORDER BY last_name ASC, cust_last_name ASC;

-- 2. Listado de las categorías con más de dos productos obsoletos (PRODUCT_STATUS = 'obsolete').
--    Lista la categoría y el número de productos obsoletos
SELECT category_id, count(*)
FROM pr5_product_information
WHERE product_status = 'obsolete'
GROUP BY category_id
HAVING count(*) > 2;

-- 3. Se quiere generar un "ranking" de los productos más vendidos en el último semestre del año 1990. Para ello nos piden
--    mostrar el nombre del producto y el número de unidades vendidas para cada producto vendido en el ultimo semestre del año
--    1990 (ordenado por número de unidades vendidas de forma descendente).
SELECT product_name, count(*)
FROM pr5_orders JOIN pr5_order_items USING (order_id) JOIN pr5_product_information USING (product_id)
WHERE order_date BETWEEN '01-JUN-1999' AND '31-DEC-1999'
GROUP BY product_id,product_name
ORDER BY count(*) DESC;

-- 4. Muestra los puestos de la empresa que tienen un salario mínimo superior al salario medio de los empleados de la compañía.
--    El listado debe incluir el puesto y su salario mínimo, y estar ordenado ascendentemente por salario mínimo.
SELECT job_id, job_title, min_salary
FROM pr5_jobs
WHERE min_salary > (
    SELECT avg(salary) 
    FROM pr5_employees
)
ORDER BY min_salary ASC;

-- 5. Mostrar el código, nombre y precio mínimo de productos de la categoría 14 que no aparecen en ningún pedido.
--    Usa para ello una subconsulta no correlacionada
SELECT product_id, product_name, min_price
FROM pr5_product_information
WHERE product_id NOT IN (
    SELECT product_id FROM pr5_order_items
) AND category_id = 14;

-- 6. Mostrar el código de cliente, nombre y apellidos de aquellos clientes alemanes (nls_territory = 'GERMANY') que no han
--    realizado ningún pedido. Usa para ello una ¿¿consulta correlacionada.?? No se me ocrurre como hacerlo así
SELECT customer_id, cust_first_name, cust_last_name
FROM pr5_customers
WHERE nls_territory = 'GERMANY'
    AND customer_id NOT IN (SELECT customer_id FROM pr5_orders);

-- 7. Mostrar el código de cliente, nombre y apellidos (sin repetición) de aquellos clientes que han realizado al menos un 
--    pedido online (order_mode = 'online') y otro direct.
SELECT customer_id, cust_first_name, cust_last_name
FROM pr5_customers
WHERE customer_id IN (
    SELECT customer_id
    FROM pr5_orders
    WHERE order_mode = 'online'
)
AND customer_id IN (
    SELECT customer_id
    FROM pr5_orders
    WHERE order_mode = 'direct'
);

-- 8. Mostrar el nombre y apellidos de aquellos clientes que, habiendo realizado algún pedido, nunca han realizado pedidos
--    de tipo direct.
SELECT customer_id, cust_first_name, cust_last_name
FROM pr5_customers
WHERE customer_id IN (
    SELECT customer_id
    FROM pr5_orders
)
AND customer_id NOT IN (
    SELECT customer_id
    FROM pr5_orders
    WHERE order_mode = 'direct'
);

-- 9. Se quiere generar un listado de los productos que generan mayor beneficio. Mostrar el código de producto, su precio
--    mínimo, su precio de venta al público y el porcentaje de incremento de precio. En el listado deben aparecer sólo
--    aquellos cuyo precio de venta al público ha superado en un 30% al precio mínimo.
SELECT product_id, product_name, list_price, min_price, list_price/min_price*100-100
FROM pr5_product_information
WHERE min_price <> 0 AND list_price/min_price > 1.3;

-- 10.Mostrar el apellido de los empleados que ganen un 35% más del salario medio de su puesto. El listado debe incluir el
--    salario del empleado y su puesto.
SELECT last_name, job_title, salary
FROM pr5_employees emp JOIN pr5_jobs ON (pr5_jobs.job_id = emp.job_id)
WHERE salary > (
    SELECT avg(salary)
    FROM pr5_jobs JOIN pr5_employees ON (pr5_jobs.job_id = pr5_employees.job_id)
    WHERE emp.job_id = pr5_jobs.job_id
);