-- David Dav� Lavi�a
-- Enrique de la Calle Montilla
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- ARBITROS
INSERT INTO lf_personas (dni, nombre) VALUES ('11111111A', 'David P�rez Pallas');
INSERT INTO lf_personas (dni, nombre) VALUES ('22222222B', 'Alexandre Alem�n P�rez');
INSERT INTO lf_personas (dni, nombre) VALUES ('33333333C', 'Mois�s Mateo Monta��s');
INSERT INTO lf_personas (dni, nombre) VALUES ('44444444D', 'Adri�n D�az Gonz�lez');
INSERT INTO lf_personas (dni, nombre) VALUES ('55555555E', 'Juan Manuel L�pez Amaya');
INSERT INTO lf_personas (dni, nombre) VALUES ('66666666F', 'Iv�n Gonz�lez Gonz�lez');
INSERT INTO lf_personas (dni, nombre) VALUES ('77777777G', 'Jorge Figueroa V�zquez');

INSERT INTO lf_arbitros (dni, numTemp) VALUES ('11111111A', 10);
INSERT INTO lf_arbitros (dni, numTemp) VALUES ('22222222B', 2);
INSERT INTO lf_arbitros (dni, numTemp) VALUES ('33333333C', 5);
INSERT INTO lf_arbitros (dni, numTemp) VALUES ('44444444D', 1);
INSERT INTO lf_arbitros (dni, numTemp) VALUES ('55555555E', 23);
INSERT INTO lf_arbitros (dni, numTemp) VALUES ('66666666F', 15);
INSERT INTO lf_arbitros (dni, numTemp) VALUES ('77777777G', 3);

-- ENTRENADORES
INSERT INTO lf_personas (dni, nombre) VALUES ('01111110A', 'Zinedine Zidane');
INSERT INTO lf_personas (dni, nombre) VALUES ('02222221B', 'Luis Enrique Martinez Garc�a');
INSERT INTO lf_personas (dni, nombre) VALUES ('03333332C', 'Diego Simeone');
INSERT INTO lf_personas (dni, nombre) VALUES ('00000001P', 'Entrenador Fantasma');

INSERT INTO lf_entrenadores (dni) VALUES ('01111110A');
INSERT INTO lf_entrenadores (dni) VALUES ('02222221B');
INSERT INTO lf_entrenadores (dni) VALUES ('03333332C');
INSERT INTO lf_entrenadores (dni) VALUES ('00000001P');

-- EQUIPOS
INSERT INTO lf_equipos (nif, nombre, presupuesto, dniEntrenador) VALUES ('B84030576', 'Real Madrid C.F.', 4530000000, '01111110A');
INSERT INTO lf_equipos (nif, nombre, presupuesto, dniEntrenador) VALUES ('G8266298', 'F.C. Barcelona', 157000000, '02222221B');
INSERT INTO lf_equipos (nif, nombre, presupuesto, dniEntrenador) VALUES ('A80373764', 'Atl�tico de Madrid', 140000000, '03333332C');

INSERT INTO lf_equipos (nif, nombre, presupuesto, dniEntrenador) VALUES ('A55662345', 'Equipo Fantasma', 0, '00000001P');

-- JUGADORES
-- nombre puede ser NULL pues no hemos especificado una restricci�n NOT NULL
INSERT INTO lf_personas (dni) VALUES ('11111110A');
INSERT INTO lf_personas (dni) VALUES ('22222221B');
INSERT INTO lf_personas (dni) VALUES ('33333332C');
INSERT INTO lf_personas (dni) VALUES ('44444443D');
INSERT INTO lf_personas (dni) VALUES ('55555554E');
INSERT INTO lf_personas (dni) VALUES ('66666665F');
INSERT INTO lf_personas (dni) VALUES ('77777779G');

INSERT INTO lf_jugadores (dni, dorsal, ficha, demarcacion, nifEquipo) VALUES ('11111110A', 7, '32000000', 'Delantero', 'B84030576');
INSERT INTO lf_jugadores (dni, dorsal, ficha, demarcacion, nifEquipo) VALUES ('22222221B', 19, '6760000', 'CentroCampista', 'B84030576');
INSERT INTO lf_jugadores (dni, dorsal, ficha, demarcacion, nifEquipo) VALUES ('33333332C', 2, '4000000', 'Defensa', 'B84030576');
INSERT INTO lf_jugadores (dni, dorsal, ficha, demarcacion, nifEquipo) VALUES ('44444443D', 3, '5800000', 'Defensa', 'G8266298');
INSERT INTO lf_jugadores (dni, dorsal, ficha, demarcacion, nifEquipo) VALUES ('55555554E', 7, '4000000', 'CentroCampista', 'G8266298');
INSERT INTO lf_jugadores (dni, dorsal, ficha, demarcacion, nifEquipo) VALUES ('66666665F', 19, '1000000', 'Defensa', 'A80373764');
INSERT INTO lf_jugadores (dni, dorsal, ficha, demarcacion, nifEquipo) VALUES ('77777779G', 1, '3500000', 'Portero', 'A80373764');

-- PARTIDOS
INSERT INTO lf_partidos (jornada, estadio, diaYHora, nifLocal, nifVisitante, dniArbitro) VALUES
    (14, 'Camp Nou', TIMESTAMP '2016-12-03 16:15:00', 'G8266298', 'B84030576', '11111111A');
INSERT INTO lf_partidos (jornada, estadio, diaYHora, nifLocal, nifVisitante, dniArbitro) VALUES
    (11, 'Vicente Calder�n', TIMESTAMP '2016-11-19 20:45:00', 'A80373764', 'B84030576', '66666666F');
INSERT INTO lf_partidos (jornada, estadio, diaYHora, nifLocal, nifVisitante, dniArbitro) VALUES
    (11, 'Ram�n S�nchez Pizjuan', TIMESTAMP '2016-11-19 20:45:00', 'A55662345', 'G8266298', '33333333C');
INSERT INTO lf_partidos (jornada, estadio, diaYHora, nifLocal, nifVisitante, dniArbitro) VALUES
    (24, 'Vicente Calder�n', TIMESTAMP '2017-02-25 16:15:00', 'A80373764', 'G8266298', '33333333C');
    
-- ACTAS
INSERT INTO lf_actas (id_acta, dniArbitro, jornada, estadio) VALUES (00000001, '11111111A', 14, 'Camp Nou');
INSERT INTO lf_actas (id_acta, dniArbitro, jornada, estadio) VALUES (00000002, '66666666F', 11, 'Vicente Calder�n');
-- INCIDENCIAS
-- ARBSECUNDARIO
INSERT INTO lf_personas (dni, nombre) VALUES ('66666655F', 'Arbitro Fantasma');
INSERT INTO lf_arbitros (dni) VALUES ('66666655F');

INSERT INTO lf_arbsecundario (dniArbitro, jornada, estadio) VALUES ('11111111A', 14, 'Camp Nou');
INSERT INTO lf_arbsecundario (dniArbitro, jornada, estadio) VALUES ('66666666F', 11, 'Vicente Calder�n');
INSERT INTO lf_arbsecundario (dniArbitro, jornada, estadio) VALUES ('66666655F', 24, 'Vicente Calder�n');
-- INTERVIENEN_SANCIONES

